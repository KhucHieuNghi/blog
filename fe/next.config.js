/* eslint-disable @typescript-eslint/no-var-requires */
const locales = ['vi-VN', 'en-US'];
const defaultLocale = 'vi-VN';

module.exports = {
  // next config
  env: {
    basePath: process.env.BASE_PATH || '',
  },
  basePath: process.env.BASE_PATH || '',
  i18n: {
    locales,
    defaultLocale,
    localeDetection: false,
  },
  async rewrites() {
    return [
      ...locales.filter((locale) => locale !== defaultLocale).map((locale) => [
        { source: `/${locale}{/}?`, destination: '/' },
        { source: `/${locale}/:path*`, destination: '/:path*' },
      ]).reduce((acc, cur) => [...acc, ...cur], []),
    ];
  },
  async redirects() {
    return [
      {
        source: `/${defaultLocale}{/}?`,
        destination: '/',
        permanent: true,
      },
      {
        source: `/${defaultLocale}/:path*`,
        destination: '/:path*',
        permanent: true,
      },
    ];
  },
  eslint: {
    // Warning: Dangerously allow production builds to successfully complete even if
    // your project has ESLint errors.
    ignoreDuringBuilds: true,
  },
};
