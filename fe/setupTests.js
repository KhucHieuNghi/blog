/* eslint-disable no-undef */
// optional: configure or set up a testing framework before each test
// if you delete this file, remove `setupFilesAfterEnv` from `jest.config.js`

// used for __tests__/testing-library.js
// learn more: https://github.com/testing-library/jest-dom
import '@testing-library/jest-dom/extend-expect';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

// Mock window.matchMedia
beforeEach(() => {
  Object.defineProperty(window, 'matchMedia', {
    writable: true,
    value: jest.fn().mockImplementation((query) => ({
      matches: false,
      media: query,
      onchange: null,
      addListener: jest.fn(), // deprecated
      removeListener: jest.fn(), // deprecated
      addEventListener: jest.fn(),
      removeEventListener: jest.fn(),
      dispatchEvent: jest.fn(),
    })),
  });
});

jest.setTimeout(30000);

jest.mock('react-intl', () => {
  const formatMessage = ({ id }) => id;
  return {
    FormattedMessage: formatMessage,
    useIntl: () => ({ formatMessage }),
  };
});

jest.mock('use-resize-observer', () => () => ({ width: 100 }));

// https://testing-library.com/docs/example-findByText
// https://testing-library.com/docs/ecosystem-user-event/
// https://github.com/testing-library/react-hooks-testing-library
