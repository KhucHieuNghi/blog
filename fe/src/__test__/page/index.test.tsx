import { render } from '@testing-library/react';
import { Page } from '../../pages';

test('Index Page', () => {
  const { getByText } = render(<Page />);
  const linkElement = getByText('common.hotline');
  expect(linkElement).toBeInTheDocument();
});
