import { render, screen } from '@testing-library/react';
import PageContainer, { Loader } from '~/components/layout/PageContainer';

jest.mock('next/router');

test('PageContainer', () => {
  render(
    <PageContainer height={0}>
      PageContainer
    </PageContainer>,
  );

  expect(screen.getByText('PageContainer')).toBeInTheDocument();
});

test('Loader', () => {
  const { container } = render(
    <Loader />,
  );

  expect(container.lastChild).toMatchSnapshot();
});
