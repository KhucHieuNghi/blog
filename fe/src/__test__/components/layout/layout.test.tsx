import { render, act } from '@testing-library/react';
import { useRouter } from 'next/router';
import Layout from '~/components/layout';

jest.mock('next/router');

test('Layout snapshot', async () => {
  useRouter.mockReturnValue({
    locale: 'vi-VN',
  });

  act(() => {
    const { container } = render(
      <Layout />,
    );
    expect(container.firstChild).toMatchSnapshot();
  });
});
