import { renderHook } from '@testing-library/react-hooks';

import { useProvideAuth, useAuth, onCompleteRenderEmbed } from '~/components/auth/authHook';
import { customerApi } from '~/services';

jest.mock('~/services');

test('useAuth', async () => {
  const { result } = renderHook(() => useAuth());
  /* Test Function */
  expect(JSON.stringify(result.current)).toBe('{}');
});
test('useProvideAuth', async () => {
  customerApi.getCustomerProfile.mockResolvedValue({});
  const { result, waitForNextUpdate } = renderHook(() => useProvideAuth());
  await waitForNextUpdate(() => {
    expect(result.current.user).toBe({});
  });

  /* Test Function */
  expect(await result.current.signIn()).toBe(null);
  expect(await result.current.signUp()).toBe(null);
  expect(await result.current.signOut()).toBe(null);
});

test('onCompleteRenderEmbed', () => {
  /* Case 1 Get no SVG Document */
  const contextNoSVGDocument = {
    target: {
      getSVGDocument: jest.fn().mockReturnValue(false),
    },
  };
  const contextNoSVGDocumentResult = onCompleteRenderEmbed(contextNoSVGDocument);
  expect(contextNoSVGDocumentResult).toBeFalsy();

  /* Case 2 Get has SVG Document and hasn't location URL in context */
  const locationNoQuery = {
    location: {
      search: '',
    },
  };
  const contextHasSVGDocumentNoLocation = {
    target: {
      getSVGDocument: jest.fn().mockReturnValue(locationNoQuery),
    },
  };
  const contextHasSVGDocumentNoLocationResult = onCompleteRenderEmbed(contextHasSVGDocumentNoLocation);
  expect(contextHasSVGDocumentNoLocationResult).toBeFalsy();

  /* Case 3 Get has SVG Document and hasn't location URL in context */

  jest.spyOn(window, 'window', 'get').mockImplementation(() => ({
    location: {
      reload: jest.fn().mockReturnValue(true),
    },
  }));

  const locationHasQuery = {
    location: {
      search: '?code=123',
    },
  };
  const contextPassHappyCase = {
    target: {
      getSVGDocument: jest.fn().mockReturnValue(locationHasQuery),
    },
  };

  const contextPassHappyCaseResult = onCompleteRenderEmbed(contextPassHappyCase);
  expect(contextPassHappyCaseResult).toBeTruthy();
});
