import { render, act, screen } from '@testing-library/react';
import { customerApi } from '~/services';

import { AuthProvider, withLogin } from '~/components/auth';
import { useProvideAuth, useAuth } from '~/components/auth/authHook';

jest.mock('~/services');
jest.mock('~/components/auth/authHook');

const resultProfileMock = 1;

beforeEach(() => {
  customerApi.getCustomerProfile.mockResolvedValue(resultProfileMock);
  useProvideAuth.mockReturnValue({});
  useAuth.mockReturnValue({});
});

test('AuthProvider generation', () => {
  act(() => {
    const { getByText } = render(
      <AuthProvider configSSR={{}}>
        AuthProvider
      </AuthProvider>,
    );
    const linkElement = getByText('AuthProvider');
    expect(linkElement).toBeInTheDocument();
  });
});

describe('withLogin', () => {
  const ComponentMock = () => <div>ComponentMock</div>;

  test('Loading first render', async () => {
    useAuth.mockReturnValue({ loading: true });

    const Component = withLogin()(ComponentMock);
    const { container } = render(
      <Component />,
    );
    expect(container.firstChild).toMatchSnapshot();
  });

  test('Embed none user info', async () => {
    const Component = withLogin()(ComponentMock);
    render(
      <Component />,
    );
    const frameLoginElement = screen.getByTestId('frameLogin');
    expect(frameLoginElement).toBeInTheDocument();
  });

  test('Component render', async () => {
    useAuth.mockReturnValue({ user: {} });

    const Component = withLogin()(ComponentMock);
    render(
      <Component />,
    );

    const ComponentMockElement = screen.getByText('ComponentMock');
    expect(ComponentMockElement).toBeInTheDocument();
  });
});
