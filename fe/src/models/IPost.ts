import { IUser } from './IUser';

export interface IPreviewImageUrl {
    original: string
    large: string
    medium: string
    small: string
}
export interface IPost {
    id: string | number
    title: string
    description: string
    authorId: string
    updatedBy: IUser
    isPublished: boolean
    updatedAt: string
    createdAt: string
    previewImageUrl?: string
    totalLikes: number
    groupId: string
}

export interface IInputPost {
    title: string
    previewImageUrl: string
    groupId: string| number
    description: string
    userId: string | number,
}
