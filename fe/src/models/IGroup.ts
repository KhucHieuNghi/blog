export interface IGroup{
    id: string | number;
    groupName: string;
    isActive: number;
}