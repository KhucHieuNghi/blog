export interface IUser {
    avatar?: string
    fullName: string
    birthday?: string | Date,
    gender?: number,
    phone?: string,
    email: string
    description?: string
    role: string
  }
