import { Fragment, useState } from 'react';
import {
  Table, Toggle, Modal, Button, Input,
} from 'rsuite';

const {
  Column, HeaderCell, Cell,
} = Table;

const Group = ({ data }: {data: []}) => {
  const [open, setOpen] = useState<boolean>(false);
  const [, setForm] = useState<string | undefined>();

  const openForm = () => {
    setOpen(true);
    setForm(undefined);
  };

  const onOk = () => {
    setOpen(false);
  };

  return (
    <Fragment>
      <Button onClick={openForm} appearance="primary">Add</Button>
      <Table
        height={500}
        data={data}
      >
        <Column width={100}>
          <HeaderCell>ID</HeaderCell>
          <Cell dataKey="id" />
        </Column>
        <Column width={700}>
          <HeaderCell>Group Name</HeaderCell>
          <Cell dataKey="groupName" />
        </Column>
        <Column width={120}>
          <HeaderCell>Action</HeaderCell>
          <Cell>
            {() => <Toggle size="md" checkedChildren="Active" unCheckedChildren="UnActive" />}
          </Cell>
        </Column>
      </Table>
      <Modal show={open} onHide={() => setOpen(false)}>
        <Modal.Header>
          <Modal.Title>Add Group</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Input placeholder="Ex: Group Name" />
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={onOk} appearance="primary">
            Ok
          </Button>
        </Modal.Footer>
      </Modal>
    </Fragment>
  );
};

export default Group;
