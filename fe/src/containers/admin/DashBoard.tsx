import { Table } from 'rsuite';

const {
  Column, HeaderCell, Cell,
} = Table;

const Dashboard = ({ data }: {data: []}) => (
  <Table
    height={500}
    data={data}
  >
    <Column width={100}>
      <HeaderCell>ID</HeaderCell>
      <Cell dataKey="id" />
    </Column>
    <Column width={700}>
      <HeaderCell>Title</HeaderCell>
      <Cell dataKey="title" />
    </Column>
    <Column width={120}>
      <HeaderCell>Total Likes</HeaderCell>
      <Cell dataKey="totalLikes" />
    </Column>

  </Table>
);

export default Dashboard;
