/* eslint-disable jsx-a11y/anchor-is-valid */
import { format } from 'date-fns';
import { useState } from 'react';
import { Checkbox, Table, Toggle } from 'rsuite';
import { IPost } from '~/models/IPost';

const {
  Column, HeaderCell, Cell,
} = Table;

const AllPosts = ({ data }: {data: []}) => {
  const [value, setValue] = useState<IPost[]>(() => {
    const drafts = Object.values(JSON.parse(localStorage.getItem('draft') || '{}'));
    return [...data, ...drafts] as IPost[];
  });
  const publish = (id, isPublished) => {
    const newValue = value.map((item: IPost) => {
      if (item?.id === id) return ({ ...item, isPublished });
      return item;
    });
    setValue(newValue);
  };
  return (
    <Table
      height={500}
      data={value}
    >
      <Column width={500}>
        <HeaderCell>Title</HeaderCell>
        <Cell dataKey="title" />
      </Column>
      <Column width={120}>
        <HeaderCell>createdAt</HeaderCell>
        <Cell dataKey="createdAt">
          {({ createdAt }) => (createdAt ? format(new Date(createdAt), 'MM/dd/yyyy') : '')}
        </Cell>
      </Column>

      <Column width={120}>
        <HeaderCell>updatedAt</HeaderCell>
        <Cell dataKey="updatedAt">
          {({ updatedAt }) => (updatedAt ? format(new Date(updatedAt), 'MM/dd/yyyy') : '')}
        </Cell>
      </Column>

      <Column width={220} fixed="right">
        <HeaderCell>Action</HeaderCell>

        <Cell>
          {(rowData) => ((
            <span>
              <a> View </a>
              {' | '}
              <a> Edit </a>
              {' | '}
              {
                rowData.isDraft ? <Checkbox inline disabled checked> Is Draft</Checkbox> : (<Toggle size="md" checked={rowData.isPublished} checkedChildren="Publish" unCheckedChildren="Disable" onChange={(isCheck) => publish(rowData.id, isCheck)} />)
              }
            </span>
          ))}
        </Cell>
      </Column>
    </Table>
  );
};

export default AllPosts;
