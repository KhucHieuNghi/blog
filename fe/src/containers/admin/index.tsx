import styled from '@emotion/styled';
import { useState } from 'react';
import { IoIosDocument } from 'react-icons/io';
import { MdDashboard, MdGroupWork } from 'react-icons/md';
import { SiGoogletagmanager } from 'react-icons/si';
import {
  Container, Content, Header, Nav, Sidebar, Sidenav,
} from 'rsuite';
import { posts } from '~/mocks';
import DashBoard from './DashBoard';
import GroupPost from './GroupPost';
import Groups from './Groups';
import AllPosts from './MyPosts';

const Components = {
  1: DashBoard,
  2: GroupPost,
  3: AllPosts,
  4: Groups,
};

const ComponentKeys = {
  1: 'DashBoard',
  2: 'Group Post',
  3: 'All Posts',
  4: 'Groups',
};

const HeaderStyled = styled.div`
  padding: 20px;
  font-size: 16px;
  background: #1675e0;
  color: #fff;
`;

const ContainerContentStyled = styled(Container)`
    .rs-header {
        padding: 0 20px;
    }
    .rs-content{
        margin: 20px;
        padding: 20px;
    }
`;
const Admin = () => {
  const [active, setActive] = useState<string | number>(1);
  const Component = Components[active];
  return (
    <div className="show-fake-browser sidebar-page">
      <Container>
        <Sidebar collapsible style={{ display: 'flex', flexDirection: 'column' }} width={256}>
          <Sidenav activeKey={active} expanded appearance="subtle" onSelect={setActive}>
            <Sidenav.Header>
              <HeaderStyled>Setting</HeaderStyled>
            </Sidenav.Header>
            <Sidenav.Body>
              <Nav>
                <Nav.Item eventKey="1" icon={<MdDashboard />}>
                  Dashboard
                </Nav.Item>
                <Nav.Item eventKey="3" icon={<IoIosDocument />}>
                  My Post
                </Nav.Item>
                <Nav.Item eventKey="2" icon={<MdGroupWork />}>
                  Group Post
                </Nav.Item>
                <Nav.Item eventKey="4" icon={<SiGoogletagmanager />}>
                  Group
                </Nav.Item>
              </Nav>
            </Sidenav.Body>
          </Sidenav>
        </Sidebar>
        <ContainerContentStyled>
          <Header>
            <h2>{ComponentKeys[active]}</h2>
          </Header>
          <Content>
            <Component data={posts.data} />
          </Content>
        </ContainerContentStyled>
      </Container>
    </div>
  );
};

export default Admin;
