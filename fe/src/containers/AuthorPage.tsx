import { Fragment } from 'react';
import PostPreview from '~/components/shared/PostReview';
import { IHomeProps } from '~/pages';

const Author = ({ posts }: IHomeProps) => (
  <Fragment>
    {
      posts.map((post) => (
        <PostPreview
          key={post.id}
          {...post}
        />
      ))
    }
  </Fragment>
);

export default Author;
