import { Fragment } from 'react';
import { Placeholder } from 'rsuite';
import PostPreview from '~/components/shared/PostReview';
import { IHomeProps } from '~/pages';
import { usePosts } from '~/stores/postSlice';

const { Paragraph } = Placeholder;

const Home = ({ posts }: IHomeProps) => {
  const state = usePosts(posts);

  return (
    <Fragment>
      {
        state.posts?.map((post) => (
          <PostPreview
            key={post.id}
            {...post}
          />
        ))
      }
      {
        state.loading && (
          <Fragment>
            <Paragraph style={{ marginTop: 30 }} rows={5} graph="image" active />
            <Paragraph style={{ marginTop: 30 }} rows={5} graph="image" active />
            <Paragraph style={{ marginTop: 30 }} rows={5} graph="image" active />
          </Fragment>
        )
      }
    </Fragment>
  );
};
export default Home;
