import {
  Fragment, useRef, useEffect, useState,
} from 'react';
import {
  Input, Button, Uploader, IconButton, FlexboxGrid, ButtonToolbar, SelectPicker,
} from 'rsuite';
import { GrSend } from 'react-icons/gr';
import SunEditor from 'suneditor-react';
import 'suneditor/dist/css/suneditor.min.css';
import styled from '@emotion/styled';
import { useRouter } from 'next/router';
import { FileType } from 'rsuite/lib/Uploader';
import { useAuth } from '~/components/auth/authHook';
import { usePosts } from '~/stores/postSlice';

const TitleStyled = styled(Input)`
    font-size: 2.5em;
    border: none;
    resize: none !important;
    font-weight: bold;
`;

type IDraft = string | number | undefined
const saveDraft = (content:IDraft, title: IDraft, group: IDraft, img?: IDraft) => {
  if (!content || !title || !group) return;
  const draftsStr = localStorage.getItem('draft');
  const currentDate = new Date();

  if (draftsStr) {
    localStorage.removeItem('draft');
  }
  const drafts = JSON.parse(draftsStr || '{}');
  const draftOld = Object.keys(drafts)
    .slice(0, 5)
    .reduce((result, date) => {
      if (+date > currentDate.getTime()) {
        return ({ ...result, [date]: drafts[date] });
      }
      return result;
    }, {});

  currentDate.setMonth(currentDate.getMonth() + 1);
  const key = currentDate.getTime();

  const value = {
    ...draftOld,
    [key]: {
      content,
      title,
      img,
      group,
      isDraft: true,
    },
  };
  localStorage.setItem('draft', JSON.stringify(value));
};

const Write = () => {
  const router = useRouter();

  const { upCloud, user } = useAuth();
  const contentRef = useRef<string | undefined>();
  const titleRef = useRef<string>('');
  const groupRef = useRef<string | number>('');
  const [imgCover, setImgCover] = useState<string | File | undefined>('');
  const [defaultContent, setDefaultContent] = useState<string>('Tell your story...');
  const { groups, post } = usePosts();

  const onSaveDraft = async () => {
    if (!contentRef.current || !titleRef.current || !groupRef.current) return false;
    const url = imgCover && await upCloud(imgCover);
    return saveDraft(contentRef.current, titleRef.current, groupRef.current, url);
  };

  useEffect(() => {
    const content = localStorage.getItem((router.query.draftId || '').toString());
    if (content) setDefaultContent(content);

    return () => {
      if (!contentRef.current || !titleRef.current || !groupRef.current) return;
      saveDraft(contentRef.current, titleRef.current, groupRef.current);
    };
  }, []);

  const onSubmit = async () => {
    const previewImageUrl = imgCover && await upCloud(imgCover);
    const groupId = groupRef.current;
    const title = titleRef.current;
    const description = contentRef.current;
    if (!contentRef.current || !titleRef.current || !groupRef.current) return;
    post({
      previewImageUrl, groupId, title, description, userId: user.id,
    });
  };

  return (
    <Fragment>
      <FlexboxGrid justify="space-between">
        <SelectPicker
          appearance="subtle"
          data={groups}
          valueKey="id"
          labelKey="groupName"
          style={{ width: 300 }}
          placeholder="Select group name"
          onChange={(value) => { groupRef.current = value; }}
        />
        <ButtonToolbar>
          <Button size="lg" aappearance="default" onClick={onSaveDraft}>Save Draft</Button>
          <IconButton className="btn-publish" icon={<GrSend />} appearance="ghost" active onClick={onSubmit}>Publish</IconButton>
        </ButtonToolbar>
      </FlexboxGrid>
      <Uploader
        disabled={!!imgCover}
        accept="image/*"
        removable
        autoUpload={false}
        onChange={(val:FileType[]) => {
          const [file] = val;
          setImgCover(file.blobFile);
        }}
      >
        <Button appearance="subtle">Add cover photo</Button>
      </Uploader>
      <TitleStyled
        componentClass="textarea"
        rows={2}
        placeholder="Title..."
        onChange={(content) => {
          titleRef.current = content;
        }}
      />
      <SunEditor
        placeholder="Tell your story..."
        height="100vh"
        defaultValue={defaultContent}
        setDefaultStyle="font-size: 16px;"
        setOptions={{
          buttonList: [
            ['undo', 'redo'],
            ['formatBlock', 'fontSize', 'align', 'showBlocks'],
            ['bold', 'underline', 'italic'],
            ['fontColor', 'image', 'link', 'blockquote'],
            ['fullScreen', 'preview'],
          ],
        }}
        onChange={(content) => { contentRef.current = content; }}
      />
    </Fragment>
  );
};

export default Write;
