import Link from 'next/link';
import styled from '@emotion/styled';
import { IResultPagination } from '~/utils/pagination';

const Wrapper = styled.div`
    width: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    margin-top: 7rem;

    ul {
        display: flex;
    }

    li {
        cursor: pointer;
    }

    .active,
    .page,
    .placeholder {
        width: 2rem;
        margin-right: 1.5rem;
        font-size: 2rem;
    }

    .page {
        transition: color 0.2s;

        &:last-of-type {
            margin-right: 0;
        }

        &:hover {
            color: var(--color-3);
        }
    }

    .active {
        color: var(--color-3);
    }
`;

export interface IProps {
    data: IResultPagination
    active: number | string
}

/**
 * Renders the available page numbers and allows the user
 * to switch between the pages.
 * @param data {array} an array that contains all the pages. Placeholders are "..."
 * @param active {number} the currently active page
 * @returns {*}
 * @constructor
 */
const PageControls = ({ data, active }: IProps) => (
  <Wrapper>
    {
      !!data && (
        <ul>
          {(data || []).map((page) => {
            if (page === '...') {
              return <li className="placeholder" key={page}>...</li>;
            }

            return (
              <Link href={`/${page}`} key={page}>
                <li className={page === active ? 'active' : 'page'}>{page}</li>
              </Link>
            );
          })}
        </ul>
      )
    }
  </Wrapper>
);

export default PageControls;
