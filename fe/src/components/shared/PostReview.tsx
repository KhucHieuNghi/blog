/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import { css, cx } from '@emotion/css';
import Link from 'next/link';
import { useState } from 'react';
import { FcLike, FcLikePlaceholder } from 'react-icons/fc';
import { format } from 'date-fns';
import { IPost } from '~/models/IPost';

const Css = css`
    display: block;
    margin-bottom: 4rem;

    &:last-of-type {
        margin-bottom: 0;
    }
    svg{
        float: right;
        margin: 0em;
        font-size: 30px;
        cursor: pointer;
    }
`;

const SubCss = css`
    .post {
        width: 100%;
        max-width: 65rem;
        margin: 0 auto;
        border-radius: var(--border-radius);
        background-color: var(--color-2);
        box-shadow: var(--shadow);
        transition: box-shadow 0.4s;
        overflow: hidden;
        /* cursor: pointer; */

        &:hover {
            box-shadow: var(--shadow-stronger);
        }
    }

    .header {
        width: 100%;
        height: 17rem;
        background-repeat: no-repeat;
        background-size: cover;
        background-position: center;
    }

    .content {
        padding: 2.5rem;

        h2 {
            margin-bottom: 1.5rem;
        }

        div {
            font-size: 1.4rem;
            color: var(--color-2-dark);
        }

        p {
            position: relative;
            width: 100%;
            max-height: 9rem;
            margin-bottom: 1.5rem;
            text-overflow: ellipsis;
            font-weight: 500;
            overflow: hidden;

            &:after {
                content: "";
                position: absolute;
                bottom: 0;
                right: 0;
                width: 10rem;
                height: 2.2rem;
                background: linear-gradient(to right, rgba(255, 255, 255, 0), var(--color-2) 50%);
            }
        }
    }

    @media screen and (min-width: 768px) {
        .content {
            padding: 4rem;
        }
    }
`;

const PostPreview = (post: IPost) => {
  const {
    id, previewImageUrl, title, description,
  } = post;

  const [like, setLike] = useState(false);

  return (
    <div
      className={cx(
        { [Css]: true },
        { [SubCss]: true },
      )}
    >
      <article className="post">
        <div
          className="header"
          style={{ backgroundImage: `url("${previewImageUrl}")` }}
          hidden={!previewImageUrl}
        />

        <div className="content">
          <span onClick={() => setLike(true)}>
            {
              like ? <FcLike /> : <FcLikePlaceholder />
            }
          </span>
          <h2>
            <Link href="/post/[id]" as={`/post/${id}`}>
              {title}
            </Link>
          </h2>

          <p dangerouslySetInnerHTML={{ __html: description }} />
          <div>
            Last edit:
            {format(new Date(), 'MM/dd/yyyy')}
          </div>
        </div>
      </article>
    </div>
  );
};

export default PostPreview;
