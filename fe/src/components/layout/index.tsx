/* eslint-disable no-script-url */
import { PropsWithChildren } from 'react';
import PageContainer from './PageContainer';
import Nav from './Nav';
import Footer from './Footer';

interface IProps {
    showFooter?: boolean
    isDashboard?: boolean
}

const Layout = ({ children, isDashboard, showFooter = true }:PropsWithChildren<IProps>) => (
  <section className="layout-page">
    <Nav />
    <PageContainer isDashboard={isDashboard}>
      {children}
    </PageContainer>
    {
      showFooter && <Footer />
    }
  </section>

);

export default Layout;
