/* eslint-disable jsx-a11y/label-has-associated-control */
import styled from '@emotion/styled';
import { isAfter } from 'date-fns';
import Link from 'next/link';
import React, { useRef, useState } from 'react';
import { FaFacebook, FaGoogle } from 'react-icons/fa';
import {
  Button, ControlLabel, DatePicker, Drawer, FlexboxGrid, Form,
  FormControl, FormGroup, FormProps, IconButton, Radio, RadioGroup, Schema,
} from 'rsuite';
import { IUser } from '~/models/IUser';
import useDeviceDetect from '~/utils/hooks/useDeviceDetect';
import { useAuth } from '../auth/authHook';
import Logo from './Logo';

const { StringType } = Schema.Types;

const DrawerStyled = styled(Drawer)`
    svg{
        margin: auto 10px auto -24px;
    }
    form {
        padding: 1em;
        button{
            margin: 0.2em;
        }
    }

`;

const SignIn = () => {
  const formRef = useRef<FormProps>();

  const { signInGG, useFaceBookLogin, signIn } = useAuth();
  const { LoginComponent, context } = useFaceBookLogin();
  const [loading, setLoad] = useState(false);

  const submit = async () => {
    setLoad(true);
    try {
      const { hasError } = await formRef.current?.checkAsync();
      if (hasError) return;
      signIn(formRef.current?.state.formValue);
    } finally {
      setLoad(false);
    }
  };

  return (
    <React.Fragment>
      <Form
        fluid
        ref={(ref) => { formRef.current = ref; }}
        model={Schema.Model({
          email: StringType()
            .isEmail('Please enter a valid email address.')
            .isRequired('This field is required.'),
        })}
      >
        <FormGroup>
          <ControlLabel>Sign in using a secure link</ControlLabel>
          <FormControl type="email" placeholder="Enter your Email address" name="email" />
        </FormGroup>
        <Button disabled={loading} onClick={submit} appearance="ghost" active>Login</Button>
      </Form>
      <Form fluid>
        <FormGroup>
          <ControlLabel>Or, connect with</ControlLabel>
          <LoginComponent {...context} render={() => <IconButton disabled={loading} color="blue" icon={<FaFacebook />} active>Facebook</IconButton>} />
          <IconButton disabled={loading} color="red" icon={<FaGoogle />} active onClick={signInGG}>Google Plus</IconButton>
        </FormGroup>
      </Form>
    </React.Fragment>
  );
};

const Avatar = ({ action, src }:{action: (data) => void; src: string}) => (
  <div className="personal-image">
    <label className="label">
      <input type="file" accept="image/*" onChange={action} />
      <figure className="personal-figure">
        <img src={src || 'https://avatars1.githubusercontent.com/u/11435231?s=460&v=4'} className="personal-avatar" alt="avatar" />
        <figcaption className="personal-figcaption">
          <img alt="camera" src="https://raw.githubusercontent.com/ThiagoLuizNunes/angular-boilerplate/master/src/assets/imgs/camera-white.png" />
        </figcaption>
      </figure>
    </label>
  </div>
);

const Info = () => {
  const {
    user, updateUser, upCloud, signOut,
  } = useAuth();
  const [plaintext, setPlaintext] = useState<boolean>(true);
  const [formValue, setFormValue] = useState<IUser>(user);

  const uploadAvatar = async (data) => {
    const avatar = await upCloud(data.target.files[0]);
    updateUser({ avatar });
  };

  const updateProfile = () => {
    setPlaintext(true);
    updateUser(formValue);
  };

  return (
    <Form
      fluid
      formValue={formValue}
      onChange={(val) => { setFormValue(val as IUser); }}
    >
      <FormGroup>
        <ControlLabel>Photo</ControlLabel>
        <Avatar action={uploadAvatar} src={user.avatar} />
      </FormGroup>
      <FormGroup>
        <ControlLabel>Email</ControlLabel>
        <FormControl plaintext name="email" />
      </FormGroup>
      <FormGroup>
        <ControlLabel>Phone number</ControlLabel>
        <FormControl name="phone" plaintext={plaintext} />
      </FormGroup>
      <FormGroup>
        <ControlLabel>Full Name</ControlLabel>
        <FormControl name="fullName" plaintext />
      </FormGroup>
      <FormGroup>
        <ControlLabel>Gender</ControlLabel>
        <FormControl
          inline
          name="gender"
          accepter={RadioGroup}
          plaintext={plaintext}
        >
          <Radio value={0}>Female</Radio>
          <Radio value={1}>Male</Radio>
        </FormControl>
      </FormGroup>
      <FormGroup>
        <ControlLabel>Birthday</ControlLabel>
        <FormControl
          block
          name="birthday"
          plaintext={plaintext}
          disabledDate={(date) => isAfter(date, new Date())}
          accepter={DatePicker}
        />
      </FormGroup>
      <FormGroup>
        <ControlLabel>About You</ControlLabel>
        <FormControl name="description" rows={5} componentClass="textarea" plaintext={plaintext} />
      </FormGroup>
      <FormGroup>
        <ControlLabel>Links</ControlLabel>
        <Link href="/admin">
          My Dashboard
        </Link>
      </FormGroup>
      <FlexboxGrid justify="space-between">
        <FlexboxGrid.Item>
          <Button appearance="primary" active disabled={plaintext} onClick={updateProfile}>UPDATE</Button>
          <Button appearance="ghost" active disabled={!plaintext} onClick={() => setPlaintext(false)}>EDIT</Button>
        </FlexboxGrid.Item>
        <FlexboxGrid.Item>
          <Button color="red" appearance="ghost" onClick={signOut}>
            Log out
          </Button>
        </FlexboxGrid.Item>
      </FlexboxGrid>
    </Form>
  );
};

export const withDrawerLogin = (Component) => {
  const WrapperDrawer = (props) => {
    const [isOpenAuth, setOpenAuth] = useState<boolean>(false);
    const { detectMobile } = useDeviceDetect();
    const isMobile = detectMobile();
    const { user } = useAuth();

    const openAuth = () => { setOpenAuth(true); };

    return (
      <React.Fragment>
        <Component openAuth={openAuth} {...props} />
        <DrawerStyled
          full={isMobile}
          show={isOpenAuth}
          onHide={() => setOpenAuth(false)}
        >
          <Drawer.Header>
            <Drawer.Title>
              <b>
                {
                  user ? 'Profile' : 'Sign in / Create an account'
                }
              </b>
            </Drawer.Title>
          </Drawer.Header>
          <Drawer.Body>
            {
              user ? <Info /> : <SignIn />
            }
          </Drawer.Body>
        </DrawerStyled>
      </React.Fragment>
    );
  };
  return WrapperDrawer;
};

/**
 * Renders the navigation bar.
 * @returns {*}
 * @constructor
 */
const Nav = ({ openAuth }: {openAuth: () => void}) => {
  const { user } = useAuth();
  return (
    <nav className="layout-nav">
      <div className="layout-container">
        <Logo />

        <ul className="layout-menu">
          <li>
            <Link href="/">
              News
            </Link>
          </li>
          {
            !!user && (
              <li>
                <Link href="/write">
                  Write
                </Link>
              </li>
            )
          }
          <li>
            <Button appearance="primary" active onClick={openAuth}>{user ? 'Profile' : 'Login'}</Button>
          </li>
        </ul>
      </div>
    </nav>
  );
};

export default withDrawerLogin(Nav);
