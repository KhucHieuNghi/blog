import {
  FaDev, FaFacebook, FaGithub, FaInstagram, FaLinkedin, FaTwitter, FaYoutube,
} from 'react-icons/fa';
import Logo from './Logo';

/**
 * Renders social icons for the available social links.
 * You can edit the link in "blogConfig.js".
 * @returns {*}
 * @constructor
 */
const SocialIcons = () => (
  <div className="layout-social">
    <a href="/" target="_blank" rel="noreferrer">
      <FaYoutube />
    </a>

    <a href="/" target="_blank" rel="noreferrer">
      <FaLinkedin />
    </a>

    <a href="/" target="_blank" rel="noreferrer">
      <FaDev />
    </a>

    <a href="/" target="_blank" rel="noreferrer">
      <FaFacebook />
    </a>

    <a href="/" target="_blank" rel="noreferrer">
      <FaInstagram />
    </a>

    <a href="/" target="_blank" rel="noreferrer">
      <FaTwitter />
    </a>

    <a href="/" target="_blank" rel="noreferrer">
      <FaGithub />
    </a>
  </div>
);

/**
 * Renders a column with a title and links. The links can be
 * edited in "blogConfig.js".
 * @param title {string} title of the column
 * @param items {array} contains all the links with label and url
 * @returns {*}
 * @constructor
 */
const Links = ({ title, items }: {title: string, items: {label: string ; url :string}[]}) => (
  <div className="layout-links">
    <h6>{title}</h6>

    <ul>
      {items.map((link) => (
        <li key={link.label}>
          <a href={link.url} target="_blank" rel="noreferrer">{link.label}</a>
        </li>
      ))}
    </ul>
  </div>
);

const backLinks = [
  {
    title: 'Libs',
    items: [
      {
        label: 'NextJS',
        url: '/',
      },
      {
        label: 'Emotion',
        url: '/',
      },
      {
        label: 'ReactJS',
        url: '/',
      },
      {
        label: 'NodeJS',
        url: '/',
      },
    ],
  },
];

/**
 * Renders the blog's footer. The content of the footer can be
 * @returns {*}
 * @constructor
 */
const Footer = () => (
  <footer className="layout-footer">
    <div className="layout-wrapper">
      <div className="layout-info">
        <Logo />
        <p className="layout-description">A blogging platform built for developers by developers. Blog on personal domain, share ideas, and connect with the global dev community!</p>
        <SocialIcons />
      </div>
      {backLinks.map((links) => <Links key={links.title} {...links} />)}
    </div>
  </footer>
);

export default Footer;
