import { PropsWithChildren } from 'react';
import { cx, css } from '@emotion/css';

interface IProps {
    isDashboard?: boolean
}

const Css = css`
    padding: 5.5em 1em 1em 0;
    margin: auto 0;
`;

export const Loader = () => (
  <div id="loader">
    <div className="loader spinner-overlay">
      <div className="ball-grid-pulse">
        <div />
        <div />
        <div />
        <div />
        <div />
        <div />
        <div />
        <div />
        <div />
      </div>
    </div>
  </div>
);

const PageContainer = ({ children, isDashboard }:PropsWithChildren<IProps>) => (
  <main
    className={cx(
      { [Css]: isDashboard },
      'container',
    )}
  >
    {children}
  </main>
);

export default PageContainer;
