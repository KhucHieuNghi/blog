import Link from 'next/link';

/**
 * This component should render your logo.
 * Feel free to change it to your needs.
 * @returns {*}
 * @constructor
 */
const Logo = () => (
  <Link href="/">
    <div className="layout-logo">
      <img src="/images/logo.png" alt="logo" />
      <b> | BLOG</b>
    </div>
  </Link>
);

export default Logo;
