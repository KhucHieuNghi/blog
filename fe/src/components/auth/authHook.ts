import {
  createContext, useCallback, useContext, useEffect, useMemo, useState,
} from 'react';
import { useGoogleLogin } from 'react-google-login';
import { IUser } from '~/models/IUser';
import { profileApi } from '~/services';
import { FBLogin as LoginComponent } from '.';
// import { customerApi } from '~/services';

export const ROLES = {
  owner: 'OWNER',
  admin: 'ADMIN',
  member: 'MEMBER',
};

export const PAGE_SCOPES = {
  write: 'WRITE',
  admin: 'ADMIN',
};

export const PERMISSIONS = {
  [ROLES.admin]: [PAGE_SCOPES.admin, PAGE_SCOPES.write],
  [ROLES.owner]: [PAGE_SCOPES.admin, PAGE_SCOPES.write],
  [ROLES.member]: [PAGE_SCOPES.write],
};

export const AuthContext = createContext({});

export const useAuth = () => useContext(AuthContext);

export const useProvideAuth = (configSSR) => {
  const [user, setUser] = useState<IUser | undefined>();

  const getUser = useCallback(async () => {
    const res = await profileApi.getProfile();
    if (!res?.user) return;
    setUser(res.user);
  }, []);

  useEffect(() => {
    getUser();
  }, []);

  const signIn = useCallback(async (value) => {
    await profileApi.login(value);
    const res = await profileApi.getProfile(value.email);
    if (!res?.user) return;
    setUser(res.user);
  }, []);

  const { signIn: signInGG } = useGoogleLogin({
    clientId: configSSR.GG_Id,
    onSuccess: ({ profileObj }:any) => {
      signIn({
        email: profileObj?.email,
        name: profileObj?.name,
        imageUrl: profileObj?.imageUrl,
      });
    },
  });

  const useFaceBookLogin = () => ({ LoginComponent, context: { appId: configSSR.FB_Id, callback: signIn } });

  const upCloud = useCallback((data) => {
    const formData = new FormData();
    formData.append('file', data);
    formData.append('upload_preset', configSSR.CLOUD_UPLOAD_PRESET);
    formData.append('cloud_name', configSSR.CLOUD_NAME);
    return profileApi.upCloud(configSSR.CLOUD_UPLOAD_URL, formData);
  }, []);

  const signOut = useCallback(async () => {
    profileApi.logout();
    setUser(undefined);
  }, []);

  const updateUser = useCallback(async (params: IUser) => {
    const newValue = { ...user, ...params };
    await profileApi.updateProfile(newValue);
    setUser(newValue);
  }, [user]);

  const checkPageRole = useCallback((page: string):boolean => {
    if (!user) return false;
    const permissions = PERMISSIONS[user.role.toUpperCase()];
    return permissions.includes(page);
  }, [user]);

  const values = useMemo(() => ({
    user,
    signIn,
    signInGG,
    signOut,
    upCloud,
    useFaceBookLogin,
    updateUser,
    checkPageRole,
  }), [user]);

  return { ...values, signInGG, useFaceBookLogin };
};
