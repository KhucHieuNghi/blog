import {
  PropsWithChildren,
} from 'react';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';
import { IConfig } from '~/utils/contains';
import {
  AuthContext, useProvideAuth,
} from './authHook';

interface IAuthProps {
  configSSR: IConfig
}

interface ILoginFB {
    callback: (info) => void
    render: (context) => void
    appId: string
}

export const FBLogin = (props:ILoginFB) => (
  <FacebookLogin
    fields="name,email,picture"
    scope="public_profile,user_friends,user_actions.books"
    {...props}
  />
);

export const AuthProvider = ({ children, configSSR }: PropsWithChildren<IAuthProps>) => {
  const auth = useProvideAuth(configSSR);
  return (
    <AuthContext.Provider value={{ ...auth }}>
      {children}
    </AuthContext.Provider>
  );
};

export default AuthProvider;
