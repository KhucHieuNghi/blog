import { combineReducers } from 'redux';
import postSlice, { IPostState } from './postSlice';

export interface IRootState {
    post: IPostState
}

const rootReducer = combineReducers({
  post: postSlice,
});

export default rootReducer;
