import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { IGroup } from '~/models/IGroup';
import { IInputPost, IPost } from '~/models/IPost';
import { postApi } from '~/services';
import { IPostGroup } from '~/services/abstracts/post';
import { IRootState } from './rootReducer';

export interface IPostState {
    loading: boolean
    posts: IPost[]
    groups: IGroup[]
    postsFirst: IPost[]
}

export const getPostsThunk = createAsyncThunk(
  'getPosts',
  async () => {
    try {
      const responseInfo: IPostGroup = await postApi.getPosts();
      return { posts: responseInfo.post, groups: responseInfo.group };
    } catch (error) {
      return null;
    }
  },
);

export const postThunk = createAsyncThunk(
  'getPosts',
  async (param:IInputPost, { dispatch }) => {
    await postApi.createPost(param);
    dispatch(getPostsThunk());
  },
);

const initialState:IPostState = {
  loading: false,
  posts: [],
  groups: [],
  postsFirst: [],
};

export const postSlice = createSlice({
  name: 'post',
  initialState,
  reducers: {
    firstPosts: (state, action) => ({ ...state, postsFirst: action.payload }),
  },
  extraReducers: (builder) => {
    builder
      .addCase(getPostsThunk.fulfilled, (state, action) => ({
        ...state,
        ...action.payload,
        loading: false,
      }))
      .addCase(getPostsThunk.pending, (state) => ({
        ...state,
        loading: true,
      }));
  },
});

export const usePosts = (posts?: IPost[]) => {
  const dispatch = useDispatch();
  const state = useSelector((prop: IRootState) => prop.post);
  const post = (params) => dispatch(postThunk(params));
  useEffect(() => {
    dispatch(postSlice.actions.firstPosts(posts));
    dispatch(getPostsThunk());
  }, []);
  return { ...state, post };
};

export default postSlice.reducer;
