import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';
import { getCookie } from './cookie';

export * from 'axios';

const request = axios.create({
  timeout: 5000,
  headers: {
    'x-envoy-upstream-rq-timeout-ms': 30000,
    'Content-Type': 'application/json',
  },
});

export const promiseHandler = (fn, ...context) => fn(...context)
  .then((res) => res.data.data)
  .catch(() => undefined);

export function createRequestInterceptor() {
  return async function interceptor(config: AxiosRequestConfig) {
    // Derived config from dynamic config

    const access_token = getCookie('access_token');

    if (access_token) {
      return {
        ...config,
        headers: {
          ...config.headers,
          access_token,
          Authorization: `Bearer ${access_token}`,
        },
      };
    }

    return {
      ...config,
      headers: {
        ...config.headers,
      },
    };
  };
}

// *Axios define interface error is any
type IHandlerChain = (error:any, next:IHandlerChain) => void

export function createHandlerChain(handlers:IHandlerChain[]) {
  return function handlerChain(error: any) {
    const {
      config: { useDefaultErrorHandler = true },
    } = error;
    if (!useDefaultErrorHandler) {
      return Promise.reject(error);
    }
    const stack = [...handlers];

    function next():void {
      if (stack.length === 0) {
        return;
      }
      const nextHandler: IHandlerChain | undefined = stack.pop();
      nextHandler?.(error, next);
    }

    next();
    return Promise.reject(error);
  };
}

// *Axios define interface error is any
export const serverErrorHandler = (error:any, next: any) => {
  // TODO check cases error
  next();
};

export function parseResultsHandler(response: AxiosResponse) {
  return response.data;
}

request.interceptors.request.use(createRequestInterceptor());
request.interceptors.response.use(
  parseResultsHandler,
  createHandlerChain([
    serverErrorHandler,
  ]),
);

export default request;
