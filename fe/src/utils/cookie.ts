/* eslint-disable no-param-reassign */
import { serialize, CookieSerializeOptions } from 'cookie';
import { NextApiResponse } from 'next';

export const setCookie = (name:string, value: string, days?: number) => {
  if (typeof window === 'undefined') return false;
  let expires = '';
  if (days) {
    const date = new Date();
    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
    expires = `; expires=${date.toUTCString()}`;
  }
  document.cookie = `${name}=${value || ''}${expires}; path=/`;
  return true;
};
export const getCookie = (name: string) => {
  if (typeof window === 'undefined') return false;

  const matches = document.cookie.match(new RegExp(
    // eslint-disable-next-line no-useless-escape
    `(?:^|; )${name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1')}=([^;]*)`,
  ));
  return matches ? decodeURIComponent(matches[1]) : false;
};

export const formatCookieToken = (
  name: string,
  value: unknown,
  options: CookieSerializeOptions = {},
):string => {
  const stringValue = typeof value === 'object' ? `j:${JSON.stringify(value)}` : `${value}`;

  if ('maxAge' in options) {
    options.expires = new Date(Date.now() + options.maxAge);
    options.maxAge /= 1000;
  }

  return serialize(name, String(stringValue), options);
};

export const setCookiesServer = (res: NextApiResponse, values: string) => {
  res.setHeader('Set-Cookie', values);
};

export const parseCookie = (cookieParam:string) => {
  const cookies = cookieParam.split(';');
  return cookies.reduce((result, cookie: string) => {
    const [key, value] = cookie.split('=');
    return { ...result, [key.trim()]: value.trim() };
  }, {});
};
