const promiseHandler = (fn, ...context) => fn(...context)
  .then((res) => res.data)
  .catch(() => undefined);

export default promiseHandler;
