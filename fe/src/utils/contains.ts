export interface IConfig {
    [key: string] : string
}

export const config:IConfig = {
  development: {
    GG_Id: '1065827823916-5stuiou0ktc2b19s7hnsqsfivhamkdnk.apps.googleusercontent.com',
    FB_Id: '2356087434434892',
    URL: 'http://localhost:3000',
    API: 'http://localhost:3001',
    CLOUD_NAME: 'ducjtiovh',
    CLOUD_UPLOAD_PRESET: 'jcuf6qb1',
    CLOUD_UPLOAD_URL: 'https://api.cloudinary.com/v1_1/ducjtiovh/image/upload',
    ...process.env,
  },
  production: {
    ...process.env,
  },
  test: {
    ...process.env,
  },
}.development;

export const LANG = {
  VI: 'vi-VN',
  EN: 'en-US',
};
