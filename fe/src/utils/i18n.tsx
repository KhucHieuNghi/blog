import { ReactNode } from 'react';
import { IntlProvider, FormattedMessage } from 'react-intl';
import { useRouter, NextRouter } from 'next/router';
import en from '~/lang/en';
import vi from '~/lang/vi';
import { LANG } from './contains';

interface ILang {
    'vi-VN': Record<string, string>
    'en-US': Record<string, string>
    'vi': Record<string, string>
    'en': Record<string, string>
}

const languages:ILang = {
  'vi-VN': vi,
  'en-US': en,
  vi,
  en,
};

type IProps = {
    children: ReactNode | ReactNode[]
}

interface IFormatMessageIntl{
    id: string
    values?: IEmpty
    router?: NextRouter
}

const useI18n = (router: Partial<NextRouter> = {}) => {
  const { locale = LANG.VI, defaultLocale } = router;
  const messages = languages[locale];
  const Component = ({ children }:IProps) => (
    <IntlProvider messages={messages} locale={locale} defaultLocale={defaultLocale} onError={() => null}>
      {children}
    </IntlProvider>
  );
  return Component;
};

const I18N = ({ children }:IProps) => {
  const router = useRouter();
  const Component = useI18n(router);
  return (
    <Component>
      {children}
    </Component>
  );
};

export const FormatMessageIntl = ({
  router,
  id,
  values = {},
}: IFormatMessageIntl) => {
  try {
    const Component = useI18n(router);
    return (
      <Component>
        <FormattedMessage id={id} values={values} defaultMessage="Lỗi hệ thống" />
      </Component>
    );
  } catch {
    const [lang] = window?.location.pathname.split('/');
    if (lang === LANG.EN) return languages[lang][id];
    return languages[LANG.VI][id];
  }
};

export default I18N;
