const getDevice = (userAgent: NavigatorID['userAgent']) => {
  const detectAndroid = () => Boolean(userAgent.match(/Android/i));
  const detectIos = () => Boolean(userAgent.match(/iPhone|iPad|iPod/i));
  const detectOpera = () => Boolean(userAgent.match(/Opera Mini/i));
  const detectWindows = () => Boolean(userAgent.match(/IEMobile/i));
  const detectSSR = () => Boolean(userAgent.match(/SSR/i));
  const detectMobile = () => Boolean(detectAndroid() || detectIos() || detectOpera() || detectWindows());
  const detectDesktop = () => Boolean(!detectMobile() && !detectSSR());
  return {
    detectMobile,
    detectDesktop,
    detectAndroid,
    detectIos,
    detectSSR,
  };
};

const useDeviceDetect = () => {
  const userAgent = typeof navigator === 'undefined' ? 'SSR' : navigator.userAgent;
  return getDevice(userAgent);
};

export default useDeviceDetect;
