import axios from 'axios';
import querystring from 'querystring';
import { Method } from '~/utils/request';
import { LANG } from '../contains';
import { formatCookieToken, parseCookie } from '../cookie';

export const HTTP_STATUS = {
  OK: 200,
  INTERNAL_ERROR: 500,
};

export const refreshToken = async (cookies, baseUrl) => {
  if (cookies.token || !cookies.refreshtoken) return undefined;
  const body = {
    query: `
            mutation {
            refresh{
                    token
                    refreshtoken
                    expiredToken
                    expiredRefreshToken
                }
            }
        `,
  };
  const token = await axios.post(`${baseUrl}/refresh`, body, {
    headers: {
      refreshtoken: cookies.refreshtoken,
    },
  });
  return token.data.data;
};

export const baseURL = (locale = LANG.VI) => {
  const { origin } = window.location;
  if (locale === LANG.VI) return origin;
  return `${origin}/${locale}`;
};

const responseSuccess = (res, data) => {
  res.status(200).json({
    isSuccess: true,
    data,
  });
};

const responseErrors = (res, status, message, data = {}) => {
  res.status(status).json({
    isSuccess: false,
    message,
    data,
  });
};

export const withMiddleware = (handler) => (req, res) => {
  res.RESP = (data = {}) => {
    if (typeof data !== 'object') {
      responseSuccess(res, data.toString());
    } else {
      responseSuccess(res, data);
    }
  };

  res.ERROR = (status = 500, message, data = {}) => {
    responseErrors(res, status, message, data);
  };

  req.getStructureDynamicRouterServer = async (baseUrl: string) => {
    const {
      method: methodParam, query, body: data, headers,
    } = req;
    const { slugs, ...other } = query;
    const headersNew = { ...headers };

    const cookies:{[key:string]: string} = parseCookie(headersNew.cookie);

    const newToken = await refreshToken(cookies, baseUrl);
    if (newToken) {
      cookies.token = formatCookieToken('token', newToken.token);
    }

    const newCookie: string = Object.entries(cookies).map((cookie) => cookie.join('=')).join(';');

    delete headersNew.host;

    const method = methodParam as Method;

    const path = [...slugs].join('/');
    const queryParams = querystring.stringify(other);

    const isRequestAuth: boolean = ['login', 'refresh', 'logout'].includes(slugs[0]);

    return {
      url: `${baseUrl}/${path}?${queryParams}`,
      method,
      data,
      headers: { ...headersNew, cookies: newCookie },
      isRequestAuth,
      newToken,
    };
  };

  return handler(req, res);
};

export default withMiddleware;
