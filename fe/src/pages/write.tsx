import dynamic from 'next/dynamic';
import { useAuth } from '~/components/auth/authHook';

const Component = dynamic(() => import('~/containers/WritePage'), {
  ssr: false,
});

const page = 'WRITE';

const Page = () => {
  const { checkPageRole } = useAuth();
  if (!checkPageRole(page)) return 'No Permission';
  return <Component />;
};

Page.showFooter = false;

export default Page;
