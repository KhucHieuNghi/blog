import HomePage from '~/containers/HomePage';
import { posts } from '~/mocks';
import { IPost } from '~/models/IPost';

export interface IHomeProps {
    posts: IPost[]
    currentPage?: string
}

export const Page = (props:IHomeProps) => <HomePage {...props} />;

export default Page;

export const getStaticProps = async (ctx) => {
  const currentPage = +(ctx?.params?.currentPage || '1');

  return { props: { posts: posts.data, currentPage } };
};
