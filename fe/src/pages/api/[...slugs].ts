import type { NextApiRequestCustom, NextApiResponseCustom } from 'next';
import { config } from '~/utils/contains';
import { formatCookieToken, setCookiesServer } from '~/utils/cookie';
import withMiddleware from '~/utils/hooks/withMiddleware';
import request from '~/utils/request';

const handler = async (req:NextApiRequestCustom, res: NextApiResponseCustom) => {
  try {
    const { newToken, isRequestAuth, ...configRequest } = await req.getStructureDynamicRouterServer(config.API);
    const result = await request({
      ...configRequest,
    });

    if (isRequestAuth || newToken) {
      const data = result.data?.login || result.data?.logout || newToken.refresh;
      const {
        token, refreshtoken, expiredToken, expiredRefreshToken,
      } = data;
      setCookiesServer(res, [formatCookieToken('token', token, {
        httpOnly: true,
        path: '/',
        maxAge: expiredToken,
      }), formatCookieToken('refreshtoken', refreshtoken, {
        httpOnly: true,
        path: '/',
        maxAge: expiredRefreshToken,
      })]);
    }

    res.RESP(result.data);
  } catch (e) {
    res.ERROR(500, 'Lỗi', {});
  }
};

export default withMiddleware(handler);
