import '../styles/index.scss';
import { AppProps, AppContext } from 'next/app';
import Head from 'next/head';
import { createWrapper } from 'next-redux-wrapper';
import { Provider } from 'react-redux';
import store from '~/stores/rootStores';
import Layout from '~/components/layout';
import I18N from '~/utils/i18n';
import AuthProvider from '~/components/auth';
import { config as configSSR } from '~/utils/contains';

export type StaticInitProp = AppProps & AppContext & {
    Component
}
function MyApp({ Component, pageProps }: StaticInitProp) {
  const layoutProps = {
    showFooter: Component.showFooter,
    isDashboard: Component.isDashboard,
  };
  return (
    <AuthProvider configSSR={pageProps.configSSR}>
      <I18N>
        <Head>
          <title>My Blog</title>
          <meta name="viewport" content="width=device-width,initial-scale=1" />
        </Head>
        <Provider store={store}>
          <Layout {...layoutProps}>
            <Component {...pageProps} />
          </Layout>
        </Provider>
      </I18N>
    </AuthProvider>
  );
}

MyApp.getInitialProps = async ({ Component, ctx }: StaticInitProp) => {
  const pageProps = Component.getInitialProps ? await Component.getInitialProps(ctx) : {};
  return {
    pageProps: {
      ...pageProps,
      configSSR,
      locale: ctx.locale,
    },
  };
};

const makeStore = () => store;

const Wrapper = createWrapper(makeStore);

export default Wrapper.withRedux(MyApp);
