import Component from '~/containers/AuthorPage';
import { posts } from '~/mocks';

const Page = (props) => (
  <Component
    {...props}
  />
);

export const getStaticProps = async (ctx) => {
  const currentPage = +(ctx?.params?.currentPage || '1');

  return { props: { posts: posts.data, currentPage } };
};

export default Page;
