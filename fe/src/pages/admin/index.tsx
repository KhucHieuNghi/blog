import dynamic from 'next/dynamic';
import { useAuth } from '~/components/auth/authHook';

const Component = dynamic(() => import('~/containers/admin'), {
  ssr: false,
});

const page = 'ADMIN';

const Page = () => {
  const { checkPageRole } = useAuth();

  if (!checkPageRole(page)) return 'No Permission';
  return <Component />;
};

Page.isDashboard = true;

export default Page;
