import { IGroup } from '~/models/IGroup';
import { IPost } from '~/models/IPost';

export interface IPostParams {
    readonly page: number
    readonly perPage: number
}

export interface IPostGroup {
    post: IPost[]
    group: IGroup[]
}

export default abstract class PostAbstract {
    abstract getPosts(params: IPostParams): Promise<IPostGroup>;
}
