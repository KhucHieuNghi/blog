import { IUser } from '~/models/IUser';

export default abstract class ProfileAbstract {
    abstract getProfile(email: string);

    abstract updateProfile(user: IUser): Promise<IResponseAxios<IUser>>;
}
