import { IInputPost, IPost } from '~/models/IPost';
import promiseHandler from '~/utils/promise';
import request from '~/utils/request';
import PostAbstract, { IPostGroup } from '../abstracts/post';

export default class PostApi extends PostAbstract {
  async getPosts(): Promise<IPostGroup> {
    const body = {
      query: `
            query {
                post {
                    id
                    title
                    previewImageUrl
                    description
                    isPublished
                    totalLikes
                    userId
                    groupId
                    updatedAt
                    createdAt
                }
            group {
                id
                groupName
            }
            }
        `,
    };
    return promiseHandler(request.post, '/api/graphql', body);
  }

  async createPost({
    title, previewImageUrl, groupId, description, userId,
  }:IInputPost): Promise<IResponseAxios<IPost>> {
    const body = {
      query: `
        mutation {
            createPost(title: "${title}", previewImageUrl: "${previewImageUrl}", groupId: ${+groupId}, description: "${description}", userId: ${+userId}){
                id
            }
        }
        `,
    };
    return promiseHandler(request.post, '/api/graphql', body);
  }

  async getGroups(): Promise<IResponseAxios<IPost>> {
    const body = {
      query: `
            query {
              
            }
        `,
    };
    return promiseHandler(request.post, '/api/graphql', body);
  }
}
