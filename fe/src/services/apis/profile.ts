import axios from 'axios';
import { IUser } from '~/models/IUser';
import promiseHandler from '~/utils/promise';
import request from '~/utils/request';
import ProfileAbstract from '../abstracts/profile';

export default class ProfileApi extends ProfileAbstract {
  async getProfile(email?: string) {
    const body = {
      query: `
              query {
                user(email: "${email || ''}"){
                    id
                    fullName
                    birthday
                    gender
                    phone
                    email
                    description
                    avatar
                    updatedAt
                    createdAt
                    role
                  }
              }
          `,
    };
    return promiseHandler(request.post, '/api/graphql', body);
  }

  async login(headers) {
    const body = {
      query: `
            mutation {
                login{
                    token
                    refreshtoken
                    expiredToken
                    expiredRefreshToken
                }
            }
        `,
    };
    await request.post('/api/login', body, {
      headers,
    });
    return headers.email;
  }

  async logout() {
    const body = {
      query: `
            mutation {
                logout{
                    token
                    refreshtoken
                    expiredToken
                    expiredRefreshToken
                }
            }
        `,
    };
    await request.post('/api/logout', body);
  }

  async updateProfile(params): Promise<IResponseAxios<IUser>> {
    const body = {
      query: `
            mutation {
                updateUser(id: ${params.id}, gender: ${params.gender}, phone: "${params.phone || ''}", description: "${params.description || ''}", avatar: "${params.avatar || ''}") {
                        fullName
                    }
                }
        `,
    };
    return request.post('/api/graphql', body);
  }

  async upCloud(host, data): Promise<string> {
    const res = await axios.post(host, data);
    return res.data.url;
  }
}
