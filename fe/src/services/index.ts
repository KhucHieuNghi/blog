import Post from './apis/post';
import Profile from './apis/profile';

export const postApi = new Post();
export const profileApi = new Profile();
