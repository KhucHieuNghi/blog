How to run?
-> yarn && yarn dev (For developer) | npm install & npm install -g lint-staged
* file script in package.json
* Formatter use Extension in VSCode Install "Eslint"

How to run product?
-> yarn build
-> yarn start:server (http request all) || yarn start:static (ssr, http request only nextJS)

- lib
   * @emotion/style SCSS
   * React redux (tool kid)
   * React Hook
   * NextJs
   * Intl
   * Axios
   * Moment 
    Eslint


components ---- reuseable component
container ---- coding component
pages ---- router in nextJS (read document)
styles ---- css, less, scss global
service ---- request API
lang ---- locale
mock ---- dumpy
models ---- interface, type in typescript
store ---- side effect, redux,...
utils ---- file contains, function base...
static


NextJs 
- Universal App
- static page
- seo
- dynamic page
- dynamic router
- lazy img
- support AMP

..........


