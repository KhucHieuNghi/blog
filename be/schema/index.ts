import { gql } from 'apollo-server-express';

// linkSchema merge schema utils from
const Schemas = gql`
    scalar Date

    enum Role {
        admin
        owner
        member
      }

    type Query {
        _: Boolean
    }

    input InputUSER {
        id: Int
        fullName: String
        birthday: String
        gender: Int
        phone: String
        email: String
        description: String
        avatar: String
        updatedAt: String
        createdAt: String
        role: String
    }

    input InputPOST {
        title: String
        description: String
        previewImageUrl: String
        isPublished: Int
        totalLikes: Int
        userId: ID
        groupId: ID
        updatedAt: String
        createdAt: String
    }

    input InputGROUP {
        groupName: String
        isActive: Int
        updatedAt: String
        createdAt: String
    }

    type TOKEN {
        token: String
        refreshtoken: String
        expiredToken: Int
        expiredRefreshToken: Int
    }

    type Mutation {
        login: TOKEN
        refresh: TOKEN
        logout: TOKEN
        createPost(title: String!, previewImageUrl: String, groupId: Int, description: String, userId: Int): POST
        createUser(data: InputUSER): USER
        updateUser(id: Int!, gender: Int, phone: String, description: String, avatar: String): USER
        createGroup(data: InputGROUP): GROUP
    }

    type Subscription {
        _: Boolean
    }
    type Query {
        post: [POST]
        user(email: String): USER
        group(active: Int): [GROUP]
    }

    type POST {
        id: String
        title: String
        description: String
        previewImageUrl: String
        isPublished: Int
        totalLikes: Int
        userId: ID
        groupId: ID
        updatedAt: String
        createdAt: String
    }

    type USER {
        id: ID
        fullName: String
        birthday: String
        gender: Int
        phone: String
        email: String
        description: String
        avatar: String
        updatedAt: String
        createdAt: String
        role: String
        posts: [POST]
    }

    type GROUP {
        id: ID
        groupName: String
        isActive: Int
        updatedAt: String
        createdAt: String
    }

    type React  {
        id: ID
        postId: ID
        userId: ID
      }
      
    type PermissionGroup  {
        id: ID
        userId: ID
        groupId: ID
    }

`;

export default [
    Schemas, 
];
