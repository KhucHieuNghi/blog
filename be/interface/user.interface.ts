import { Model } from "sequelize/types";
import { IPost } from "./post.interface";

export interface IUser {
  id?: string | number;
  fullName: string;
  name: string;
  birthday: string;
  gender: number;
  phone: string;
  email: string;
  description: string;
  avatar: string;
  updatedAt: string;
  createdAt: string;
  role: string;
  imageurl: string;
  posts?: [IPost];
}

export type IUserModal = Model<IUser>;
