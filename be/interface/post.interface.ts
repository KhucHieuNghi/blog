import { Model } from "sequelize";

export interface IPost {
  id?: string | number;
  title: string;
  description: string;
  previewImageUrl: string;
  isPublished: number;
  totalLikes: number;
  userId: number | string;
  groupId: number | string;
  updatedAt: string;
  createdAt: string;
}

export interface IInputPost {
    userId: string | number
}

export type IPostDefine = IPost & Model;

export type IPostModal = Model<IPost>[];
