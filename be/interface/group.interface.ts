import { Model } from "sequelize";

export interface IGroup {
  id: string | number;
  groupName: string;
  isActive: number;
}

export type IInputGroup = IGroup;

export type IGroupDefine = IGroup & Model;

export type IGroupsModal = Model<IGroup>[];
export type IGroupModal = Model<IGroup>;
