import "dotenv/config";
import { ApolloServer, ServerInfo } from "apollo-server";
import { Request, Response } from 'express'
import { GroupDataSource } from "./connectors/group.connect";
import { PostDataSource } from "./connectors/post.connect";
import { UserDataSource } from "./connectors/user.connect";
import sequelize from "./models";
import resolvers from "./resolver";
import schema from "./schema";
import { promiseVerify } from "./utils";

const port = process.env.PORT || 3001;

sequelize.authenticate().then(() => console.log("connection success"));

interface IContext {
    req: Request
    res: Response
}

const getContext = async ({req}: IContext) => {
  const token = (req.headers.token || "").toString();

  if (!token) return { isAuth: false, ...req.headers };

  const isAuth = !!(await promiseVerify(
    token,
    process.env.TOKEN_KEY || "TOKEN_KEY"
  ));

  return { token, isAuth };
};

const server = new ApolloServer({
  typeDefs: schema,
  resolvers,
  cors: true,
  context: async (props) => await getContext(props),
  dataSources: () => ({
    PostDataSource: new PostDataSource(),
    UserDataSource: new UserDataSource(),
    GroupDataSource: new GroupDataSource(),
  }),
});

server.listen({ port }).then(({ url }: ServerInfo) => {
  console.log(`🚀  Server ready at ${url}`);
});
