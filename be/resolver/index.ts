import { AuthenticationError } from "apollo-server";
import { GraphQLDateTime } from "graphql-iso-date";
import {
  clearToken,
  getAllToken,
  IToken,
  parseCookie,
  promiseVerify,
} from "../utils";
import type {
  IGroupModal,
  IGroupsModal,
  IInputGroup,
} from "../interface/group.interface";
import { IInputPost, IPost } from "interface/post.interface";
import { IConnectors } from "connectors/connectors.interface";
import { IUser } from "interface/user.interface";

const customScalarResolver = {
  Date: GraphQLDateTime,
};

type IContext = {
  dataSources: IConnectors;
  isAuth: boolean;
  cookie: string;
} & IUser & IToken;

type IEmpty = {
  [key: string]: Record<string, unknown> | Record<string, never>;
};

export default [
  customScalarResolver,
  {
    Query: {
      post: async (_: IEmpty, args: IInputPost, context: IContext) => {
        const { dataSources } = context;
        const { rows: data } = await dataSources.PostDataSource.getAll();
        return data;
      },
      user: async (_: IEmpty, args: IUser, context: IContext) => {
        const { email } = args;
        const { dataSources, cookie } = context;
        const { refreshtoken }: IToken = parseCookie(cookie.toString());
        const user = await promiseVerify(
          refreshtoken,
          process.env.REFRESH_TOKEN_KEY || "REFRESH_TOKEN_KEY"
        );

        if (!user && !email) return null;

        const data = await dataSources.UserDataSource.getByEmail(
          user?.email || ""
        );
        return data;
      },
      group: async (
        parent: IEmpty,
        args: IInputGroup,
        context: IContext
      ): Promise<IGroupsModal> => {
        const { dataSources } = context;
        return await dataSources.GroupDataSource.getAll();
      },
    },
    Mutation: {
      createGroup: async (
        parent: IEmpty,
        data: IInputGroup,
        { dataSources, isAuth }: IContext
      ): Promise<IGroupModal> => {
        if (!isAuth) throw new AuthenticationError("you must be logged in");
        return await dataSources.GroupDataSource.addGroup(data);
      },
      createUser: async (
        _: IEmpty,
        data: IUser,
        { dataSources, isAuth }: IContext
      ) => {
        if (!isAuth) throw new AuthenticationError("you must be logged in");
        return await dataSources.UserDataSource.addUser(data);
      },
      updateUser: async (
        _: IEmpty,
        data: IUser,
        { dataSources, isAuth }: IContext
      ) => {
        if (!isAuth) throw new AuthenticationError("you must be logged in");
        return await dataSources.UserDataSource.updateUser(data);
      },
      createPost: async (
        _: IEmpty,
        data: IPost,
        { dataSources, isAuth }: IContext
      ) => {
        if (!isAuth) throw new AuthenticationError("you must be logged in");
        return await dataSources.PostDataSource.addPost(data);
      },
      login: async (
        _: IEmpty,
        __: IEmpty,
        { dataSources, ...other }: IContext
      ) => {
        const { email, imageurl, name } = other;

        const user = await dataSources.UserDataSource.getByEmail(email);
        if (!user) {
          const data = {
            email,
            avatar: imageurl,
            fullName: name,
            role: "owner",
          };
          await dataSources.UserDataSource.addUser(data);
        }
        return getAllToken(email);
      },
      refresh: async (_: IEmpty, data: IEmpty, { refreshtoken }: IContext) => {
        const info = await promiseVerify(
          refreshtoken,
          process.env.REFRESH_TOKEN_KEY || "REFRESH_TOKEN_KEY"
        );
        if (!info) return clearToken();
        return getAllToken(info.email || "");
      },
      logout: () => clearToken(),
    },
  },
];
