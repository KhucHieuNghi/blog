import { IUser } from "interface/user.interface";
import jwt from "jsonwebtoken";
export interface IToken {
  token: string;
  refreshtoken: string;
  expiredToken: number;
  expiredRefreshToken: number;
}

const tokenDefault = {
  token: "",
  refreshtoken: "",
  expiredToken: -1,
  expiredRefreshToken: -1,
};

export const getToken = (email: string) => {
  const token = jwt.sign({ email }, process.env.TOKEN_KEY || "TOKEN_KEY", {
    expiresIn: "1m",
  });
  return { token, expiredToken: 6000 };
};

export const refreshToken = (email: string) => {
  const refreshtoken = jwt.sign(
    { email },
    process.env.REFRESH_TOKEN_KEY || "REFRESH_TOKEN_KEY",
    {
      expiresIn: "1d",
    }
  );
  return { refreshtoken, expiredRefreshToken: 86400000 };
};

export const getAllToken = (email: string): IToken => ({
  ...getToken(email),
  ...refreshToken(email),
});

export const clearToken = (): IToken => tokenDefault;

export const promiseVerify = (
  token: string,
  secretKey: string
): Promise<Partial<IUser> | undefined> => {
  return new Promise((res) => {
    jwt.verify(token, secretKey, (err, decoded) => {
      if (err) res(undefined);
      res(decoded as Partial<IUser>);
    });
  });
};

export const parseCookie = (cookieParam: string): IToken => {
  const cookies = cookieParam.split(";");
  return cookies.reduce((result, cookie: string) => {
    const [key, value] = cookie.split("=");
    return { ...result, [key.trim()]: value.trim() };
  }, tokenDefault);
};
