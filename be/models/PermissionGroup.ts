import { DataTypes, Sequelize } from "sequelize";

export default (sequelize: Sequelize): void => {
  sequelize.define(
    "PermissionGroup",
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      user_id: DataTypes.INTEGER,
      group_id: DataTypes.INTEGER,
    },
    {
      tableName: "PermissionGroup",
      timestamps: false,
    }
  );
};
