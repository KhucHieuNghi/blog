import { DataTypes, Sequelize } from "sequelize";

export default (sequelize: Sequelize) => {
  sequelize.define(
    "User",
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      fullName: DataTypes.STRING,
      birthday: DataTypes.STRING,
      gender: DataTypes.INTEGER,
      phone: DataTypes.STRING,
      email: DataTypes.STRING,
      description: DataTypes.STRING,
      avatar: DataTypes.STRING,
      role: DataTypes.STRING,
      createdAt: DataTypes.DATE,
      updatedAt: DataTypes.DATE,
    },
    {
      tableName: "User",
      timestamps: false,
    }
  );
};
