import { DataTypes, Sequelize } from "sequelize";

export default (sequelize: Sequelize): void => {
  sequelize.define(
    "React",
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      post_id: DataTypes.INTEGER,
      user_id: DataTypes.INTEGER,
    },
    {
      tableName: "React",
      timestamps: false,
    }
  );
};
