import {Sequelize} from 'sequelize'

// In a real app, you should keep the database connection URL as an environment variable.
// But for this example, we will just use a local SQLite database.
// const sequelize = new Sequelize(process.env.DB_CONNECTION_URL);

const applyExtraSetup = (sequelize: Sequelize) => {
	const { Post, User } = sequelize.models;
	User.hasMany(Post, {
        foreignKey: {
            name: 'userId'
          }
    });
	// instrument.belongsTo(orchestra);
}

const sequelize = new Sequelize((process.env.DB_URL || 'postgres://nilpvesj:sAxVxu5HylMdvlfLyp1pqO7lEIBIcqpX@chunee.db.elephantsql.com/nilpvesj'));

const modelDefiners = [
	require('./User'),
    require('./PermissionGroup'),
    require('./Post'),
    require('./Group'),
    require('./React'),
];

// We define all models according to their files.
for (const modelDefiner of modelDefiners) {
	modelDefiner.default(sequelize);
}

// We execute any extra setup after the models are defined, such as adding associations.
applyExtraSetup(sequelize);

export type ISequelize = typeof sequelize;

// We export the sequelize connection instance to be used around our app.
export default sequelize;