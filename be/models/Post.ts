import { DataTypes, Sequelize } from "sequelize";

export default (sequelize: Sequelize): void => {
  sequelize.define(
    "Post",
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      title: DataTypes.STRING,
      description: DataTypes.STRING,
      previewImageUrl: DataTypes.STRING,
      isPublished: DataTypes.INTEGER,
      totalLikes: DataTypes.INTEGER,
      userId: DataTypes.INTEGER,
      groupId: DataTypes.INTEGER,
      createdAt: DataTypes.DATE,
      updatedAt: DataTypes.DATE,
    },
    {
      tableName: "Post",
      timestamps: false,
    }
  );
};
