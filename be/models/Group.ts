import { DataTypes, Sequelize } from "sequelize";
import { IGroupDefine } from "../interface/group.interface";

export default (sequelize: Sequelize): void => {
  sequelize.define<IGroupDefine>(
    "Group",
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      groupName: DataTypes.INTEGER,
      isActive: DataTypes.INTEGER,
    },
    {
      tableName: "Group",
      timestamps: false,
    }
  );
};
