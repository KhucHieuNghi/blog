import { GroupDataSource } from "./group.connect";
import { PostDataSource } from "./post.connect";
import { UserDataSource } from "./user.connect";

export interface IConnectors {
  PostDataSource: PostDataSource
  UserDataSource: UserDataSource;
  GroupDataSource: GroupDataSource;
}
