import { RESTDataSource } from "apollo-datasource-rest";
import sequelize from "../models";
import { IUser, IUserModal } from "../interface/user.interface";

export class UserDataSource extends RESTDataSource {
  constructor() {
    super();
  }

  async getByEmail(email: string): Promise<IUserModal | null> {
    return sequelize.models.User.findOne({
      where: { email },
    });
  }

  async addUser(data: Partial<IUser>) {
    return sequelize.models.User.create(data);
  }

  async updateUser(user: Partial<IUser>) {
    const { id, ...data } = user;
    return sequelize.models.User.update(data, {
      where: {
        id,
      },
    });
  }
}
