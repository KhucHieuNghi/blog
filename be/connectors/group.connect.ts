import { RESTDataSource } from "apollo-datasource-rest";
import type {
  IGroup,
  IGroupModal,
  IGroupsModal,
} from "../interface/group.interface";
import sequelize from "../models";
export class GroupDataSource extends RESTDataSource {
  constructor() {
    super();
  }

  async getAll(): Promise<IGroupsModal> {
    const res = await sequelize.models.Group.findAll();
    return res;
  }

  async addGroup(data: IGroup): Promise<IGroupModal> {
    return sequelize.models.Group.create(data);
  }
}
