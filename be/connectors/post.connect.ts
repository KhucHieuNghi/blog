import { RESTDataSource } from "apollo-datasource-rest";
import { IPost } from "../interface/post.interface";
import sequelize from "../models";

export class PostDataSource extends RESTDataSource {
  constructor() {
    super();
  }

  async getAll() {
    return sequelize.models.Post.findAndCountAll({
      where: {
        isPublished: 1,
      },
      order: [["createdAt", "desc"]],
    });
  }

  async addPost(post: IPost) {
    return sequelize.models.Post.create(post);
  }
}
